#include <iostream>

using namespace std;

class Massiv
{
public:

    Massiv()
    {
        Size = 5;
        cout << "MassivCreated" << endl;
        Mas = new int* [Size];
        for (int i = 0; i < Size; i++)
        {
            Mas[i] = new int;
        }
        Info = new string("MasInfoEmpty");
        Name = new string("MasNameEmpty");
    }
    Massiv(int size)
    {
        Size = size;
        cout << "MassivCreated" << endl;
        Mas = new int*[Size];
        for (int i = 0; i < Size; i++)
        {
            Mas[i] = new int;
        }
        Info = new string("MasInfoFilled");
        Name = new string("MasNameFilled");
    }

    Massiv(const Massiv& other)
    {
        cout << "MassivCopied" << endl;
        if (other.Mas)
        {
            if (Mas)
            {
                for (int i = 0; i < Size; i++)
                {
                    delete[] Mas[i];
                }
                delete[] Mas;
            }
            Size = other.Size;
            Mas = new int* [Size];
            for (int i = 0; i < Size; i++)
            {
                Mas[Size] = new int[Size];
            }
        }
        if (other.Info)
        {
            if (Info) delete Info;
            Info = new string(*(other.Info));
        }
        if (other.Name)
        {
            if (Name) delete Name;
            Name = new string(*(other.Name));
        }
    }

    Massiv& operator=(Massiv& m1)
    {
        cout << "Operator = Copied " << endl;
        if (m1.Mas)
        {
            if (Mas)
            {
                for (int i = 0; i < Size; i++)
                {
                    delete[] Mas[i];
                }
                delete[] Mas;
            }
            Size = m1.Size;
            Mas = new int* [Size];
            for (int i = 0; i < Size; i++)
            {
                Mas[Size] = new int[Size];
            }
        }
        if (m1.Info)
        {
            if (Info) delete Info;
            Info = new string(*(m1.Info));
        }
        if (m1.Name)
        {
            if (Name) delete Name;
            Name = new string(*(m1.Name));
        }

        return (*this);
    }

    string printname()
    {
        return *this->Name;
    }

private:
    int Size;
    int** Mas;
    string* Info;
    string* Name;
};

int main()
{
    Massiv m1(10);
    Massiv m2 = m1;
    Massiv m3;
    m3 = m2;
}
